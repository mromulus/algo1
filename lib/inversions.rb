InversionCounter = Struct.new(:input) do
  attr_reader :count

  def count!
    @count = 0
    merge_sort_and_count(casted_input)
  end

  private

  def casted_input
    input.map(&:to_i)
  end

  def merge_sort_and_count(array)
    return array if array.size <= 1

    merge_and_count(*split_and_sort(array))
  end

  def split_and_sort(array)
    array.each_slice((array.size/2.0).ceil).map do |half|
      merge_sort_and_count(half.dup)
    end
  end

  def merge_and_count(left, right)
    [].tap do |array|
      while !left.empty? && !right.empty?
        if left.first <= right.first
          array << left.shift
        else
          array << right.shift
          @count += left.size
        end
      end

      array.concat left
      array.concat right
    end
  end
end
